FROM python:3.8
 
# RUN apt-get update -y
# RUN apt-get install -y python-pip
 
COPY . /gradioapp
 
# Create and change to the app directory.
WORKDIR /gradioapp
 
RUN chmod 444 app.py
RUN chmod 444 requirements.txt
 
#RUN pip install -r requirements.txt
#RUN pip install gradio>=3.36.1
RUN pip install -r requirements.txt
 
# Service must listen to $PORT environment variable.
# This default value facilitates local development.
EXPOSE 8080
 
# Run the web service on container startup.
CMD [ "python", "app.py" ]
