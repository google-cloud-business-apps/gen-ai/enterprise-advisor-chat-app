import gradio as gr

#Langchain
import langchain
print(f"LangChain version: {langchain.__version__}")
from langchain.llms import VertexAI
from langchain import PromptTemplate, LLMChain

#Initialization
PROJECT_ID = "Your Project Id"

LOCATION = "us-central1"  # @param {type:"string"}


#from vertexai.preview.language_models import (TextGenerationModel)

template = """Question: {question}
Answer: Let's think step by step."""
prompt = PromptTemplate(template=template, input_variables=["question"])

from google.cloud import aiplatform
print(f"Vertex AI SDK version: {aiplatform.__version__}")

# Initialize Vertex AI SDK
import vertexai
vertexai.init(project=PROJECT_ID, location=LOCATION)

#Get ChatModel
from vertexai.preview.language_models import ChatModel, InputOutputTextPair


chat_model = ChatModel.from_pretrained("chat-bison@001")
parameters = {
    "max_output_tokens": 256,
    "temperature": 0.8,
    "top_p": 0.8,
    "top_k": 40
}
chat = chat_model.start_chat(
    context="""Your name is Denny. You are an advisor who is knowledgeable about all things labels and RFID.
Respond in short sentences. Shape your response as if talking to a 10-years-old.""",
    examples=[
        InputOutputTextPair(
            input_text="""How many moons does Mars have?""",
            output_text="""Very good question. Mars has two moons, Phobos and Deimos. They are very small and irregularly shaped. Phobos is the larger of the two moons and is about 17 miles (27 kilometers) in diameter. Deimos is about 12 miles (19 kilometers) in diameter. Both moons are thought to be captured asteroids."""
        )
    ]
)

def greet(question):
    response = chat.send_message(question, **parameters)
    return response.text

demo = gr.Interface(fn=greet, inputs="text", outputs="text")
    
if __name__ == "__main__":
    demo.launch(server_name="0.0.0.0", server_port=8080)
