# View demo here ->. https://gradioapp-fioj224pzq-uc.a.run.app/

# Enterprise Advsisor or Enterprise Assistant Chat App

This simple app allows Enterprises to spin up a version of Bard within their tenant.

Since the UI stays within the Enterprise tenant, CISOs can feel safe that their employees can now use an agent within their security purview for activities such as:
- Summarize a document
- Rewrite a report
- Answer questions after ingesting a large repository of documentation
- Access improved response to questions

# Steps to launch the ChatApp

1. Clone this repo to your Cloud Console
2. Build using gcloud:
     gcloud builds submit --tag gcr.io/PROJECT_ID/APP_ID
3. Deploy using Cloud Run:
     gcloud run deploy APP_ID --image gcr.io/PROJECT_ID/APP_ID --allow-unauthenticated
